package string_calculator_12092018;

public class NegativesNotAllowedException extends RuntimeException {

    public NegativesNotAllowedException(String message) {
        super(message);
    }
}
