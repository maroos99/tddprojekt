package string_calculator_12092018;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringCalculator {

    public static int add(String... numbers) {
        int result = 0;
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = numbers[i].replaceAll("[\\s]+", ",");
        }
        String tab[];
        List<String> negativesNumbersList = new ArrayList<String>();
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i].equals("")) {
                numbers[i] = String.valueOf(0);
            }
            tab = numbers[i].split("[^0-9-]+");
            for (int j = 0; j < tab.length; j++) {
                if (!tab[j].equals("") && Integer.parseInt(tab[j]) < 1000) {
                    if (Integer.parseInt(tab[j]) >= 0) {
                        result += Integer.parseInt(tab[j]);
                    } else {
                        negativesNumbersList.add(tab[j]);
                    }
                }
            }
        }
        if (negativesNumbersList.size() > 0) {
            throw new NegativesNotAllowedException("negatives not allowed: " + negativesNumbersList);
        }
        return result;
    }

}
