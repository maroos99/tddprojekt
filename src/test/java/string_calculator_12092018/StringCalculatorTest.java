package string_calculator_12092018;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    @Test
    void add() {
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("");

        //Then
        assertEquals(0,result);
    }

    @Test
    void addOneNumber() {
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("1");

        //Then
        assertEquals(1,result);
    }

    @Test
    void addTwoNumbers(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("1","2");

        //Then
        assertEquals(3,result);
    }
    @Test
    void addTwoNumbersTest2(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("1,2",",2,, 3");

        //Then
        assertEquals(8,result);
    }


    @Test
    void addNewLineSeparator(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("1\n2,3");

        //Then
        assertEquals(6,result);
    }

    @Test
    void addOtherLineSeparator(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("//;\n1;2");

        //Then
        assertEquals(3,result);
    }

    @Test
    void addNegativeNumber(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When & Then
        Throwable exception = assertThrows(NegativesNotAllowedException.class, () -> StringCalculator.add("-4, 2, 5, 4, -2"));
        assertEquals(exception.getMessage(),"negatives not allowed: [-4, -2]");
    }

    @Test
    void addBiggerThan1000(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("1, 2, 5, 1000, 1020");

        //Then
        assertEquals(8,result);
    }

    @Test
    void LongSeparator(){
        //Given
        StringCalculator sc = new StringCalculator();

        //When
        int result = sc.add("//[aaa]\n1aaa2aaa3");

        //Then
        assertEquals(6,result);
    }


}